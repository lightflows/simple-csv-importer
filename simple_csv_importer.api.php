<?php

/**
 * Alter a CSV record or the entity being created from that record's data.
 *
 * @param array $record
 *   The CSV record keyed by CSV file headers.
 *
 * @param Drupal\Core\Entity\EntityInterface $entity
 *   The entity being created.
 *
 * @param array $context
 *   Additional context passed to the SimpleCSVImporter service.
 */
function hook_simple_csv_importer_row_alter(&$record, Drupal\Core\Entity\EntityInterface $entity, $context) {
  if ($entity->getEntityTypeId() === 'node') {
    // Set up nested values
    $record['body'] = [
      'value' => $record['body'],
      'format' => 'basic_html',
    ];
  }
}

/**
 * Alter the context passed in to the importer at point of submission.
 *
 * @param array $context
 * @param array $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 */
function hook_simple_csv_importer_context_alter(&$context, array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $context['unpublish_others'] = $form_state->getValue('unpublish_others');
}

/**
 * The importer has created the import batch operation.
 */
function hook_simple_csv_importer_import() {
  // Immediately index the imported content
  $index = \Drupal\search_api\Entity\Index::load('reviews');
  \Drupal\search_api\IndexBatchHelper::create($index);
}

/**
 * The importer finished.
 *
 * @param array $results
 *   Array of arrays. Keys "created" and "updated" contain the entity IDs that
 *   were added or updated, respectively. Keys "skipped" and "failed" contain
 *   the line numbers of the CSV which couldn't be imported.
 */
function hook_simple_csv_importer_finished($results) {
  $entity_ids = array_merge($results['created'], $results['updated']);

  // Do something with all of the entities which were imported
}
