Simple CSV Importer for Drupal 8
================================

Installation with Composer
--------------------------

Add the following to the repositories section of composer.json

    {
        "type": "vcs",
        "url": "git@bitbucket.org:lightflows/simple-csv-importer.git"
    }

Then install with Composer using:

    composer require lightflows/simple_csv_importer
