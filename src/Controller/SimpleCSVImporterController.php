<?php

namespace Drupal\simple_csv_importer\Controller;

use Drupal\Core\Controller\ControllerBase;

class SimpleCSVImporterController extends ControllerBase {

  public function page() {
    $form = \Drupal::formBuilder()->getForm('\Drupal\simple_csv_importer\Form\SimpleCSVImporterForm');
    $build['form'] = $form;

    $undo_form = \Drupal::formBuilder()->getForm('\Drupal\simple_csv_importer\Form\SimpleCSVImporterUndoForm');
    $build['undo_form'] = $undo_form;

    return $build;
  }
}
