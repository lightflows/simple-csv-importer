<?php

namespace Drupal\simple_csv_importer;

use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\Markup;
use Drupal\file\Entity\File;
use League\Csv\Reader;
use League\Csv\Statement;
use Symfony\Component\Validator\ConstraintViolationInterface;

class SimpleCSVImporter {

  const NON_UNIQUE_KEY = 'Key not unique among existing entities.';

  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface */
  protected $entityTypeManager;

  /** @var int The number of rows to import per batch operation. */
  protected $importPerBatchOperation;

  /**
   * SimpleCSVImporter constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;

    $this->importPerBatchOperation = 25;
  }

  /**
   * Test whether the CSV data is valid; throws \League\Csv\Exception if not.
   *
   * @param string $csv
   *   The CSV data as either a file URI or a string.
   *
   * @throws \League\Csv\Exception
   */
  public function test($csv) {
    if ($path = \Drupal::service('stream_wrapper_manager')->getTarget($csv)) {
      $reader = Reader::createFromPath(\Drupal::service('file_system')->realpath($csv));
    } else {
      $reader = Reader::createFromString($csv);
    }

    $reader->setHeaderOffset(0);
    $reader->getRecords();
  }

  /**
   * Import CSV data and generate entities.
   *
   * @param string $csv
   *   The CSV data as either a file URI or a string.
   *
   * @param string $entity_type
   *   The entity type to create.
   *
   * @param string|NULL $bundle
   *   The entity bundle to create.
   *
   * @param string|NULL $unique_key
   *   The header to use as a unique key for imported entities.
   *
   * @param array $context
   *   Additional context which will be passed back to
   *   hook_simple_csv_importer_row_alter() implementations.
   */
  public function import($csv, $entity_type, $bundle = NULL, $unique_key = NULL, $context = []) {
    $entity_type_def = $this->entityTypeManager->getDefinition($entity_type);
    $bundle_key = $entity_type_def->getKey('bundle');

    if ($bundle !== NULL && $bundle_key === FALSE) {
      throw new \RuntimeException('Unknown bundle ' . $bundle);
    }

    $batch = [
      'title' => t('Importing CSV'),
      'operations' => [
        [[__CLASS__, 'importFromReader'], [$csv, $entity_type, $bundle, $unique_key, $this->importPerBatchOperation, $context]],
      ],
      'finished' => [__CLASS__, 'importFromReaderFinished'],
    ];
    batch_set($batch);

    \Drupal::moduleHandler()->invokeAll('simple_csv_importer_import');
  }

  /**
   * Find an entity by unique key.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   * @param string $key
   * @param mixed $value
   *
   * @return \Drupal\Core\Entity\EntityInterface|NULL
   * @throws \Exception
   */
  protected static function findByUniqueKey($entity_storage, $key, $value) {
    $query = $entity_storage->getQuery()
      ->accessCheck(FALSE)
      ->condition($key, $value)
      ->range(0, 2);

    $ids = $query->execute();

    if (count($ids) > 1) {
      throw new \Exception(self::NON_UNIQUE_KEY);
    }

    return $entity_storage->load(reset($ids));
  }

  /**
   * Report errors to the user.
   *
   * @param integer $row_number
   * @param string[] $messages
   */
  protected static function reportErrors($row_number, $messages) {
    \Drupal::messenger()->addError(t('Failed to import row @row. Issues are described below:<br><br>@errors', [
      '@row' => $row_number + 1, // $row_number excludes header rows
      '@errors' => Markup::create(implode('<br>', $messages)),
    ]), 'error');
  }

  /**
   * Batch process callback.
   *
   * @param string $csv
   *  The path to the CSV file or CSV content.
   *
   * @param string $entity_type
   *  The entity type for new entities.
   *
   * @param string|NULL $bundle
   *  The bundle for new entities.
   *
   * @param string|NULL $unique_key
   *   The header to use as a unique key for imported entities.
   *
   * @param int $limit
   *  The maximum number of CSV rows to process.
   *
   * @param array $service_context
   *  Additional context which will be passed back to
   *  hook_simple_csv_importer_row_alter() implementations.
   *
   * @param array $context
   *  The batch process context.
   *
   * @see \Drupal\simple_csv_importer\SimpleCSVImporter::import()
   */
  public static function importFromReader($csv, $entity_type, $bundle, $unique_key, $limit, $service_context, &$context) {
    if (empty($context['results'])) {
      // Set defaults for progress counts
      $context['results']['created'] = [];
      $context['results']['updated'] = [];
      $context['results']['skipped'] = [];
      $context['results']['failed']  = [];

      // Add the service context to the results
      $context['results']['service_context'] = $service_context;
    }

    $entity_type_manager = \Drupal::entityTypeManager();
    $csv_file_entity = NULL;

    /** @var \Drupal\Core\Entity\EntityStorageInterface $entity_storage */
    $entity_storage = $entity_type_manager->getStorage($entity_type);

    /** @var \Drupal\Core\Entity\EntityTypeInterface $entity_type_def */
    $entity_type_def = $entity_type_manager->getDefinition($entity_type);
    $bundle_key = $entity_type_def->getKey('bundle');

    $offset = &$context['sandbox']['offset'];

    if ($path = \Drupal::service('stream_wrapper_manager')->getTarget($csv)) {
      $reader = Reader::createFromPath(\Drupal::service('file_system')->realpath($csv));

      $file_entities = \Drupal::entityQuery('file')
        ->accessCheck(FALSE)
        ->condition('uri', $csv)
        ->execute();

      if (!empty($file_entities)) {
        $csv_file_entity = File::load(reset($file_entities));
      }
    } else {
      $reader = Reader::createFromString($csv);
    }

    $reader->setHeaderOffset(0);

    if (!isset($offset)) {
      $offset = 0;
    }

    $statement = (new Statement())
      ->offset($offset)
      ->limit($limit);

    $records = $statement->process($reader);

    if ($records->count() === 0) {
      $context['finished'] = $reader->count();
      return;
    }

    if ($unique_key !== NULL) {
      $unique_keys = [];

      // Check that all given keys are unique
      foreach ($records->getRecords() as $row_number => $record) {
        $messages = [];

        if (empty($record[$unique_key])) {
          $messages = [
            t('Missing unique key'),
          ];
        }
        elseif ($previous_row = array_search($record[$unique_key], $unique_keys)) {
          $messages = [
            t('Non-unique key. Key was first seen on row @row', [
              '@row' => $previous_row + 1, // excludes header rows
            ]),
          ];
        }

        $unique_keys[$row_number] = $record[$unique_key];

        if (!empty($messages)) {
          \Drupal::messenger()->addError(t('Failed to import due to data integrity issue on row @row:<br><br>@errors', [
            '@row' => $row_number + 1, // $row_number excludes header rows
            '@errors' => Markup::create(implode('<br>', $messages)),
          ]), 'error');

          $context['results']['failed'][] = $row_number;

          // Don't do any further processing
          $context['results']['skipped'] = array_fill(0, $reader->count() - 1, -1);
          $context['finished'] = 1;
          return;
        }
      }

      unset($unique_keys);
    }

    foreach ($records->getRecords() as $row_number => $record) {
      $entity = NULL;
      $new_entity = FALSE;

      if ($unique_key !== NULL) {
        // Look for an existing entity
        try {
          $entity = self::findByUniqueKey($entity_storage, $unique_key, $record[$unique_key]);
        } catch (\Exception $e) {
          if ($e->getMessage() === self::NON_UNIQUE_KEY) {
            $messages = [
              t('Non-unique key @key', ['@key' => $unique_key]),
            ];

            self::reportErrors($row_number, $messages);
            $context['results']['failed'][] = $row_number;
            continue;
          } else {
            throw $e;
          }
        }
      }

      if (!$entity) {
        // Create a new entity
        /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
        if ($bundle !== NULL) {
          $entity = $entity_storage->create([$bundle_key => $bundle]);
        }
        else {
          $entity = $entity_storage->create();
        }

        $new_entity = TRUE;
      }

      \Drupal::moduleHandler()->alter('simple_csv_importer_row', $record, $entity, $service_context);

      if (!empty($record['_skip'])) {
        $context['results']['skipped'][] = $row_number;
        continue;
      }

      foreach ($record as $key => $value) {
        try {
          $entity->set($key, $value);
        } catch (\InvalidArgumentException $e) {}
      }

      /** @var EntityConstraintViolationListInterface $violations */
      $violations = $entity->validate();

      if ($violations->count() !== 0) {
        $messages = [];

        /** @var ConstraintViolationInterface $violation */
        foreach ($violations as $violation) {
          $messages[] = $violation->getPropertyPath() . ': ' . $violation->getMessage();
        }

        self::reportErrors($row_number, $messages);
        $context['results']['failed'][] = $row_number;
        continue;
      }

      $entity->save();
      $context['results'][$new_entity ? 'created' : 'updated'][] = $entity->id();

      if ($csv_file_entity !== NULL) {
        \Drupal::database()->insert('simple_csv_importer_imports')
          ->fields([
            'fid' => $csv_file_entity->id(),
            'entity_type' => $entity_type,
            'entity_id' => $entity->id(),
            'status' => $new_entity ? 'created' : 'updated',
            'created' => time(),
          ])
          ->execute();
      }
    }

    $offset += $limit;
    $context['finished'] = $offset / $reader->count();
  }

  /**
   * Batch finished callback.
   *
   * @see \Drupal\simple_csv_importer\SimpleCSVImporter::import()
   *
   * @param $success
   * @param $results
   * @param $operations
   */
  public static function importFromReaderFinished($success, $results, $operations) {
    \Drupal::messenger()->addMessage(t('Imported @count items: @created created, @updated updated, @skipped skipped, @failed failed.', [
      '@count' => count($results['created']) + count($results['updated']),
      '@created' => count($results['created']),
      '@updated' => count($results['updated']),
      '@skipped' => count($results['skipped']),
      '@failed'  => count($results['failed']),
    ]));

    \Drupal::moduleHandler()->invokeAll('simple_csv_importer_finished', [$results]);
  }
}
