<?php

namespace Drupal\simple_csv_importer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

class SimpleCSVImporterUndoForm extends FormBase {

  /**
   * @inheritdoc
   */
  public function getFormId() {
    return 'simple_csv_importer_undo';
  }

  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $previous_imports = $this->getPreviousImports();

    $form['previous_imports'] = [
      '#type' => 'container,'
    ];

    $form['previous_imports']['table'] = [
      '#type' => 'table',
      '#header' => [
        'filename' => $this->t('Filename'),
        'count' => $this->t('Entities imported'),
        'created' => $this->t('Date'),
        'actions' => $this->t('Actions'),
      ],
    ];

    foreach ($previous_imports as $delta => $import) {
      $file = File::load($import->fid);

      $form['previous_imports']['table'][$delta]['filename'] = [
        '#markup' => $file->getFilename()
      ];

      $form['previous_imports']['table'][$delta]['count'] = [
        '#markup' => $import->count,
      ];

      $form['previous_imports']['table'][$delta]['created'] = [
        '#markup' => \Drupal::service('date.formatter')->format($import->created),
      ];

      $form['previous_imports']['table'][$delta]['actions'] = [
        '#type' => 'submit',
        '#id' => 'undo-fid-' . $import->fid,
        '#undo_fid' => $import->fid,
        '#value' => $this->t('Undo'),
      ];
    }

    return $form;
  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();

    if (empty($triggering_element['#undo_fid'])) {
      return;
    }

    $fid = $triggering_element['#undo_fid'];

    $this->deleteEntitiesByCSVFid($fid);

    \Drupal::database()->delete('simple_csv_importer_imports')
      ->condition('fid', $fid)
      ->execute();

    \Drupal::messenger()->addMessage( $this->t('Import undo finished.') );
  }

  /**
   * Get previous imports.
   *
   * @return array
   *   An array of previous imports. An import is a class with properties: fid,
   *   created, count.
   */
  protected function getPreviousImports() {
    $previous_imports = [];

    $query = \Drupal::database()->select('simple_csv_importer_imports', 'imports');
    $query->addField('imports', 'fid');
    $query->addExpression('MIN(imports.created)', 'created');
    $query->addExpression('COUNT(imports.entity_id)', 'count');
    $query->groupBy('fid');
    $query->orderBy('created', 'DESC');

    $records = $query->execute();

    if (empty($records)) {
      return $previous_imports;
    }

    return $records->fetchAll();
  }

  /**
   * Delete entities by CSV file ID.
   *
   * @param int $fid
   *   The file ID of the CSV whose associated entities should be deleted.
   */
  protected function deleteEntitiesByCSVFid($fid) {
    $query = \Drupal::database()->select('simple_csv_importer_imports', 'imports');
    $query->fields('imports', ['entity_type', 'entity_id']);
    $query->condition('fid', $fid);

    $entities = $query->execute()->fetchAll();

    if (empty($entities)) {
      return;
    }

    $entity_ids = array_map(function($value) {
      return $value->entity_id;
    }, $entities);

    $entity_type = $entities[0]->entity_type;
    $entity_type_manager = \Drupal::service('entity_type.manager');

    /** @var \Drupal\Core\Entity\EntityStorageInterface $entity_storage */
    $entity_storage = $entity_type_manager->getStorage($entity_type);
    $entity_storage->delete($entity_storage->loadMultiple($entity_ids));
  }
}
